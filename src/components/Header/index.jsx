import { Link } from 'react-router-dom'

import './styles.css'

const Header = () => {
  return (
    <header className="header-container">
      <Link to="/">
        <h1>Trivial</h1>
      </Link>
    </header>
  )
}

export default Header
