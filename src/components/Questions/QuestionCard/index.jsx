import './styles.css'
import Choices from '../Choices'
import { useParams } from 'react-router-dom'

const QuestionCard = ({ question }) => {
  const { questionNr } = useParams()

  return (
    <div className="question-card" data-order={questionNr}>
      {/* https://react.dev/reference/react-dom/components/common#dangerously-setting-the-inner-html */}
      <div
        className="question-text"
        dangerouslySetInnerHTML={{ __html: question.question }}
      />
      <Choices
        choices={[question.correct_answer, ...question.incorrect_answers]}
      />
    </div>
  )
}

export default QuestionCard
