import './styles.css'
import { useDispatch, useSelector } from 'react-redux'
import { add_answer } from '../../../../store/slices/answers'
import { useParams, useNavigate } from 'react-router-dom'

const Choice = ({ choice }) => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { questionNr } = useParams()
  const questions = useSelector((state) => state.questions.list)

  const handleSelectChoice = () => {
    dispatch(add_answer(choice))

    const nextQuestionNr = Number(questionNr) + 1
    if (nextQuestionNr <= questions.length) {
      navigate(`/questions/${nextQuestionNr}`)
    } else {
      navigate('/result')
    }
  }

  return (
    <li
      className="choice"
      onClick={handleSelectChoice}
      dangerouslySetInnerHTML={{ __html: choice }}
    />
  )
}

export default Choice
