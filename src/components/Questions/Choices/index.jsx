import './styles.css'
import Choice from './Choice'

const Choices = ({ choices }) => {
  const getChoicesInRandomOrder = () => {
    return choices.sort(() => Math.random() - 0.5)
  }

  return (
    <ul className="choices-list">
      {getChoicesInRandomOrder().map((choice) => (
        <Choice choice={choice} key={choice} />
      ))}
    </ul>
  )
}

export default Choices
