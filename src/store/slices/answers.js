import { createSlice } from '@reduxjs/toolkit'

export const choicesSlice = createSlice({
  name: 'answers',
  initialState: {
    list: [],
  },
  reducers: {
    add_answer: (state, action) => {
      state.list.push(action.payload)
    },
    reset_answers: (state) => {
      state.list = []
    },
  },
})

export const { add_answer, reset_answers } = choicesSlice.actions

export default choicesSlice.reducer
