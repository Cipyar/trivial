import { createSlice } from '@reduxjs/toolkit'

export const questionsSlice = createSlice({
  name: 'questions',
  initialState: {
    list: [],
  },
  reducers: {
    set_questions: (state, action) => {
      state.list = action.payload
    },
    reset_questions: (state) => {
      state.list = []
    },
  },
})

export const { set_questions, reset_questions } = questionsSlice.actions

export default questionsSlice.reducer
