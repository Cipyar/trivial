import { configureStore } from '@reduxjs/toolkit'
import questionsReducer from './slices/questions'
import answersReducer from './slices/answers'

export default configureStore({
  reducer: {
    questions: questionsReducer,
    answers: answersReducer,
  },
})
