import { Link } from 'react-router-dom'

import './styles.css'

const NotFound = () => {
  return (
    <div className="not-found-container">
      <h1>404 - Oops!</h1>
      <p>This page doesn&apos;t exist.</p>
      <Link to="">Take me back!</Link>
    </div>
  )
}

export default NotFound
