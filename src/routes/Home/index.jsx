import { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch } from 'react-redux'

import './styles.css'
import { set_questions } from '../../store/slices/questions'

const Home = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    fetch(`https://opentdb.com/api.php?amount=4`)
      .then((res) => res.json())
      .then((data) => {
        dispatch(set_questions(data.results))
      })
      .catch((err) => console.log(err))
  }, [dispatch])

  return (
    <div className="home">
      <Link to="/questions/1">Start Quiz</Link>
    </div>
  )
}

export default Home
