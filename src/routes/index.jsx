import { BrowserRouter, Routes, Route } from 'react-router-dom'

import Layout from './Layout'
import Home from './Home'
import Questions from './Questions'
import Result from './Result'
import NotFound from './NotFound'

const Router = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route path="/" element={<Home />} />
          <Route path="/questions/:questionNr" element={<Questions />} />
          <Route path="/result" element={<Result />} />
          <Route path="*" element={<NotFound />} />
        </Route>
      </Routes>
    </BrowserRouter>
  )
}

export default Router
