import { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'
import QuestionCard from '../../components/Questions/QuestionCard'

const Questions = () => {
  const navigate = useNavigate()
  const { questionNr } = useParams()
  const questions = useSelector((state) => state.questions.list)

  useEffect(() => {
    // if there are no questions available, redirect to landing page
    if (!questions.length) {
      navigate('/')
    }
  }, [questions, navigate])

  return (
    <>
      {questions.length ? (
        <QuestionCard question={questions[questionNr - 1]} />
      ) : null}
    </>
  )
}

export default Questions
