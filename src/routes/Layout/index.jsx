import { Outlet } from 'react-router-dom'

import './styles.css'
import Header from '../../components/Header'

const Layout = () => {
  return (
    <div className="layout">
      <Header />
      <Outlet />
    </div>
  )
}

export default Layout
