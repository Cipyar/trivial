import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'

import './styles.css'
import ResultCard from '../../components/ResultCard'
import { reset_questions } from '../../store/slices/questions'
import { reset_answers } from '../../store/slices/answers'

const Result = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const questions = useSelector((state) => state.questions.list)
  const answers = useSelector((state) => state.answers.list)
  const numCorrectChoices = questions.filter(
    (question, i) => question.correct_answer === answers[i]
  ).length

  const handleReset = () => {
    dispatch(reset_questions())
    dispatch(reset_answers())
    navigate('/')
  }

  return (
    <div className="results">
      <h2>Results</h2>
      <div className="results-overview">{`${numCorrectChoices} / ${questions.length}`}</div>
      {/* About &#x21bb; https://developer.mozilla.org/en-US/docs/Glossary/Entity*/}
      <button onClick={handleReset} className="btn-reset">
        &#x21bb; start new game
      </button>

      {questions.length &&
        questions.map((question, i) => {
          return (
            <ResultCard
              key={question.question}
              question={question}
              choice={answers[i]}
              order={i + 1}
            />
          )
        })}
    </div>
  )
}

export default Result
