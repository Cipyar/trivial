import React from 'react'
import ReactDOM from 'react-dom/client'
import { Provider as ReduxStoreProvider } from 'react-redux'

import './index.css'
import store from './store'
import Router from './routes'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <ReduxStoreProvider store={store}>
      <Router />
    </ReduxStoreProvider>
  </React.StrictMode>
)
